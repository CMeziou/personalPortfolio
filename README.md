### Projet Portfolio Personnel - Django

## Description
Le projet Portfolio Personnel est une application web développée avec Django, un framework web Python. Il vise à présenter de manière élégante et professionnelle mes compétences, projets, et informations personnelles.

## Fonctionnalités principales
- **Page d'accueil :** Introduction personnelle et mise en avant des compétences.
- **Portfolio :** Présentation des projets réalisés avec descriptions détaillées.
- **Expérience :** Section dédiée à l'expérience professionnelle.
- **Éducation :** Informations sur le parcours académique.
- **Contact :** Formulaire de contact pour les visiteurs.
- **Administration :** Interface d'administration pour la gestion facile du contenu.


## Technologies utilisées
- Python
- Django
- HTML
- CSS
- Javascript

## Installation
1. Clonez le dépôt `git clone https://github.com/votre_utilisateur/portfolio-personnel.git`
2. Configurez la base de données dans le fichier `settings.py`.
3. Appliquez les migrations avec `python manage.py migrate`.
5. Lancez le serveur de développement avec `python manage.py runserver`.

## Auteurs
- Chems MEZIOU